core = 7.x
api = 2

; Offer Taxonomies
projects[uw_offer_taxonomies][download][type] = "git"
projects[uw_offer_taxonomies][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_offer_taxonomies.git"
projects[uw_offer_taxonomies][download][tag] = "7.x-1.1"

; Conditional Offer Blocks
projects[uw_offer_block][download][type] = "git"
projects[uw_offer_block][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_offer_block.git"
projects[uw_offer_block][download][tag] = "7.x-1.10"
